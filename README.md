docker avahi
============

# Quickstart

## Get initialize configuration:

```bash
docker create itsdaspecialk/avahi
docker cp $(docker ps -ql):/etc/avahi .
docker rm $(docker ps -ql)
```

### Disable DBus for starting the container
Edit your `avahi-daemon.conf` and set the following line.

```bash
enable-dbus=no
```

### Get help for config file
[avahi-daemon.conf man page](https://linux.die.net/man/5/avahi-daemon.conf)

# Start the container 

**NOTE:** Host networking is required, otherwise avahi will not respond to your actual ip address. This is a limitation in avahi itself.

```bash
docker run -d --net=host -v $(pwd)/avahi:/etc/avahi itsdaspecialk/avahi
```

## Issues

If you have an issue, please create an [issue on Bitbucket](https://bitbucket.org/ItsDaSpecialK/docker-avahi/issues)
