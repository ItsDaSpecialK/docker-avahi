#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
	set -- avahi-daemon "$@"
fi

if [ "$#" = 0 ]; then
	set -- avahi-daemon
fi

if [ "$1" = "avahi-daemon" ]; then
	# If Avahi didn't exit cleanly, and left a pid file,
	# then delete it.
	if [ -f "/run/avahi-daemon/pid" ]; then
		rm /run/avahi-daemon/pid
	fi
	exec "$@"
fi

exec "$@"
